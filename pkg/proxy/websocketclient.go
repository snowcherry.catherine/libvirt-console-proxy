/*
 * This file is part of the libvirt-console-proxy project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Copyright (C) 2016 Red Hat, Inc.
 *
 */

package proxy

import (
	"crypto/tls"
	"fmt"
	"net"

	"golang.org/x/net/websocket"
)

type ConsoleClientWebsocket struct {
	Tenant  net.Conn
	Compute net.Conn
}

func NewConsoleClientWebsocket(tenant *websocket.Conn, compute *net.Conn, token string, tlsConfig *tls.Config) *ConsoleClientWebsocket {

	urlStr := fmt.Sprintf("wss://console-proxy/websockify?token=%s&proxy=yes", token)
	config, err := websocket.NewConfig(urlStr, "https://localhost")
	if err != nil {
		fmt.Printf("Websocket config error: %v", err)
		return nil
	}
	config.TlsConfig = tlsConfig
	ws, err := websocket.DialConfig(config)
	if err != nil {
		fmt.Printf("Websocket dial error: %v", err)
		return nil
	}
	*compute = ws
	client := &ConsoleClientWebsocket{
		Tenant:  tenant,
		Compute: ws,
	}

	tenant.PayloadType = websocket.BinaryFrame
	ws.PayloadType = websocket.BinaryFrame

	return client
}

func (c *ConsoleClientWebsocket) proxyData(src net.Conn, dst net.Conn) error {
	data := make([]byte, 64*1024)
	pending := 0
	offset := 0
	for {
		if pending == 0 {
			var err error
			pending, err = src.Read(data)
			if err != nil {
				return err
			}
			if pending == 0 {
				return nil
			}
			offset = 0
		}

		done, err := dst.Write(data[offset:pending])
		if err != nil {
			return err
		}
		pending -= done
		offset += done
	}
}

func (c *ConsoleClientWebsocket) Close() {
	c.Tenant.Close()
	c.Compute.Close()
}

func (c *ConsoleClientWebsocket) proxyToCompute() error {
	err := c.proxyData(c.Tenant, c.Compute)
	c.Compute.Close()
	return err
}

func (c *ConsoleClientWebsocket) proxyToTenant() error {
	err := c.proxyData(c.Compute, c.Tenant)
	c.Tenant.Close()
	return err
}

func (c *ConsoleClientWebsocket) Proxy() error {
	go c.proxyToTenant()

	return c.proxyToCompute()
}
